#!/bin/bash

# Use this to just bring up all containers again if you just shut them down and want to nring them back up in the already configured Data center state

rm -rf ./build/home1/plugins/.bundled-plugins ./build/home1/plugins/.osgi-plugins ./build/home2/plugins/.bundled-plugins ./build/home2/plugins/.osgi-plugins
docker-compose up -d db jira1 jira2