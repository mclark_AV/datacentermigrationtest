#!/bin/bash

docker-compose down --remove-orphans --volumes

rm -rf build/home1/*
rm -rf build/home1/.jira-home.lock

rm -rf build/home2/*
rm -rf build/home2/.jira-home.lock

rm -rf build/sharedHome/*