# dataCenterMigrationTest
 https://confluence.atlassian.com/adminjiraserver/set-up-a-jira-data-center-cluster-993929600.html#SetupaJiraDataCentercluster-parameters

 https://confluence.atlassian.com/adminjiraserver/important-directories-and-files-938847744.html#Importantdirectoriesandfiles-Jirahomedirectory

 https://confluence.atlassian.com/jirakb/how-to-use-the-data-center-migration-app-to-migrate-jira-to-an-aws-cluster-1005781495.html

https://developer.atlassian.com/server/jira/platform/configuring-a-jira-cluster/

Possible help for Cache replication failing [here](https://confluence.atlassian.com/jirakb/jira-data-center-asynchronous-cache-replication-failing-health-check-956699182.html)

Sudden Killing of the docker container when you bring up a second node is caused by a docker memory issue. 
To fix set from docker settings from 4gb to 8gb ram and the containers should keep running
I think this is some default docker thing to kill containers to prevent OOM issues (research)[https://github.com/docker/for-win/issues/780]

