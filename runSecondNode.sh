#!/bin/bash

# 1 - copy the first nodes home directory to the mapped LOCAL home for node2
docker cp jira1:/var/atlassian/application-data/jira/. ./build/home2

# 2 - Copy node2's cluster.properties file to trigger the cluster mode and set the required configurations
cp -r ./inactiveFiles/node2/cluster.properties ./build/home2/cluster.properties

# 3 - Bring up the second node, it will boot as data center cluster mode using the shared home 
#     and as we copied the home dir from node1, this new node2 will have the dbconfig.xml file 
#     from node1 to tell it what database to connect to with all the required setup
docker-compose up -d jira2

# 4 - optional tail logs to see whats happening and when the node is up an ready
docker logs jira2 -f

# Go to http://localhost:81 for node2