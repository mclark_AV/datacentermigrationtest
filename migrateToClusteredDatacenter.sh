#!/bin/bash

# IMPORTANT You must have gone through runJiraServer.sh process first to have a server to migrate

# 1 - Run the docker cp commands below to copy the first nodes required home folders to the new shared home directory
# 2 - remember to apply data center license for Jira and scriptrunner before stopping node 1 i.e. jira1 to make this quicker and run the below 
#     before stopping as well

docker cp jira1:/var/atlassian/application-data/jira/caches ./build/sharedHome/
docker cp jira1:/var/atlassian/application-data/jira/data ./build/sharedHome/
docker cp jira1:/var/atlassian/application-data/jira/export ./build/sharedHome/
docker cp jira1:/var/atlassian/application-data/jira/import ./build/sharedHome/
docker cp jira1:/var/atlassian/application-data/jira/plugins ./build/sharedHome/
docker cp jira1:/var/atlassian/application-data/jira/logos ./build/sharedHome/

# here is where we copy the required Scriptrunner folders to maintain script files and diagnostics
docker cp jira1:/var/atlassian/application-data/jira/scripts ./build/sharedHome/
docker cp jira1:/var/atlassian/application-data/jira/scriptrunner ./build/sharedHome/

# here is where you stop jira 
docker stop jira1

# 3 - Remove .bundled-plugins and .osgi-plugins from plugins folder in node1's home and may also need to do the same for sharedHome 
#     if Atlassian plugins do not start
rm -rf ./build/home1/plugins/.bundled-plugins
rm -rf ./build/home1/plugins/.osgi-plugins

# 4 - Copy node1's cluster.properties file to trigger the cluster mode and set the required configurations
cp -r ./inactiveFiles/node1/cluster.properties ./build/home1/


# 5 - Bring up the first node again but this time it will boot as data center cluster mode using the shared home
docker-compose up -d jira1

# 6 - optional tail logs to see whats happening and when the node is up an ready
docker logs jira1 -f

# Go to http://localhost for node1

# now move to the runSecondNode.sh file
