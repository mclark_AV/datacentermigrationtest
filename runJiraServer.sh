#!/bin/bash

# 1 Run this first to bring up a standard Jira server instance and you can add a Jira server license and install scriptrunner server version
 # (when setting up the Jira DB you can refer to the database ip address as just db because the jira container resolves that to the db container) user is just `jira` and pass `jira`
 # also its a postgres db

docker-compose up -d db jira1

# 2 - Once it is up, add some scripts like listeners and some files using the script editor and make some scripts run so you have execution history

# 3 - then move to this file migrateToClusteredDatacenter.sh
